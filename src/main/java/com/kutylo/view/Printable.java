package com.kutylo.view;

@FunctionalInterface
public interface Printable {

    void print();
}
