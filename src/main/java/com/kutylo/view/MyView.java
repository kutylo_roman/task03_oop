package com.kutylo.view;

import com.kutylo.controller.Controller;
import com.kutylo.controller.ControllerImpl;
import com.kutylo.model.Bouqut;
import com.kutylo.model.Flower;
import com.kutylo.model.Store;


import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
  private Store store=new Store();
  private Controller controller;
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Scanner input = new Scanner(System.in);

  public MyView() {
    controller = new ControllerImpl();
    menu = new LinkedHashMap<>();
    menu.put("1", "  1 - print FlowerList");
    menu.put("2", "  2 - print sorted FlowerList");
    menu.put("3", "  3 - create bougut");
    menu.put("4", "  4 - print my bouquts");
    menu.put("Q", "  Q - exit");

    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::pressButton1);
    methodsMenu.put("2", this::pressButton2);
    methodsMenu.put("3", this::pressButton3);
    methodsMenu.put("4", this::pressButton4);
  }

  private void  pressButton1() {
    for(Flower flower: controller.getFlowerList()){
      System.out.println(flower);
    }
  }

  private void pressButton2() {
    for(Flower flower: controller.getSortedFlowerList()){
      System.out.println(flower);
    }
  }

  private void pressButton3() {
    store.createBougut().printBouqut();
  }

  private void pressButton4() {
    for(Bouqut bouqut:store.getBouquets()){
      bouqut.printBouqut();
    }
  }

  //-------------------------------------------------------------------------

  private void outputMenu() {
    System.out.println("\nMENU:");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println("Please, select menu point.");
      keyMenu = input.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("Q"));
  }
}
