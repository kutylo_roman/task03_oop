package com.kutylo.Utils;

import com.kutylo.model.Flower;
import com.kutylo.model.FlowerColor;
import com.kutylo.model.FlowerType;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FlowerRandomize {

    public static List<Flower> randomize(int number) {
        List<Flower> flowers = new ArrayList<Flower>();
        Random random = new Random();
        Integer price;
        Double lenght;
        Boolean isFlowerpot;
        FlowerType flowerType;
        FlowerColor flowerColor;

        for (int i = 0; i < number; i++) {
            price = random.nextInt(100);
            lenght = random.nextDouble();
            isFlowerpot=random.nextBoolean();
            flowerType = FlowerType.values()[random.nextInt(FlowerType.values().length)];
            flowerColor=FlowerColor.values()[random.nextInt(FlowerColor.values().length)];
            flowers.add(new Flower(flowerType,lenght,flowerColor,price,isFlowerpot));
        }
        return flowers;
    }

    public static void main(String[] args) {
        try {
            new FlowerIO("Inventory.json").writeToFile(FlowerRandomize.randomize(10));
        } catch (Exception e) {
            System.err.println(e);
        }
    }
}
