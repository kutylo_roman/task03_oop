package com.kutylo.Utils;
import com.fasterxml.jackson.databind.*;
import com.kutylo.model.Flower;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FlowerIO {
    private static ObjectMapper objectMapper = new ObjectMapper();

    static {
        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
    }

    private File file;

    public FlowerIO(String path) {
        this.file = new File(path);
    }

    public List<Flower> readFromFile() throws IOException {
        return objectMapper.readValue(file, objectMapper.getTypeFactory().constructCollectionType(ArrayList.class, Flower.class));
    }

    public void writeToFile(List<Flower> ammunitionList) throws IOException {
        objectMapper.writeValue(file, ammunitionList);
    }
}
