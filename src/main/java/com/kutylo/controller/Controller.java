package com.kutylo.controller;

import com.kutylo.model.Flower;

import java.util.List;

public interface Controller {
    List<Flower> getFlowerList();
    List<Flower> getSortedFlowerList();

}
