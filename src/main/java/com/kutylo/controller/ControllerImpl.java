package com.kutylo.controller;

import com.kutylo.model.Flower;
import com.kutylo.model.Store;

import java.util.List;

public class ControllerImpl implements Controller {
    private Store store;

    public ControllerImpl(){
        store=new Store();
    }

    @Override
    public List<Flower> getFlowerList(){
        return store.getInventory();
    }

    @Override
    public List<Flower> getSortedFlowerList(){
        return store.getSortedInventory();
    }
}
