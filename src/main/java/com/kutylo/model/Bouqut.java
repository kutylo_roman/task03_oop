package com.kutylo.model;

import java.util.ArrayList;
import java.util.List;

public class Bouqut {
    private List<Flower> flowers = new ArrayList<Flower>();
    private Integer totalPrice;

    public List<Flower> getFlowers() {
        return flowers;
    }

    public void setFlowers(List<Flower> flowers) {
        this.flowers = flowers;
    }

    public void addFlower(Flower flower){
        flowers.add(flower);
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
    }

    public void countTotalPrice(){
        int price=0;
        for(Flower flower:flowers){
           price+=flower.getPrice();
        }
        setTotalPrice(price);
    }

    public void printBouqut(){
        System.out.println("Your bouqut:");
        for(Flower flower:flowers){
            System.out.println(flower);
        }

        countTotalPrice();
        System.out.println("Total price: "+totalPrice);
    }

}
