package com.kutylo.model;

import java.io.Serializable;

public class Flower {
    FlowerType flowerType;
    Double length;
    FlowerColor color;
    Integer price;
    Boolean isFlowerpot;

    public Flower(){}

    public Flower(FlowerType flowerType, Double length, FlowerColor color, Integer price, Boolean isFlowerpot) {
        this.flowerType = flowerType;
        this.length = length;
        this.color = color;
        this.price = price;
        this.isFlowerpot = isFlowerpot;
    }

    public String getFlowerType() {
        return flowerType.toString();
    }

    public void setFlowerType(FlowerType flowerType) {
        this.flowerType = flowerType;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public String getColor() {
        return color.toString();
    }

    public void setColor(FlowerColor color) {
        this.color = color;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Boolean getFlowerpot() {
        return isFlowerpot;
    }

    public void setFlowerpot(Boolean flowerpot) {
        isFlowerpot = flowerpot;
    }

    @Override
    public String toString() {
        length=(double)Math.round(length*100)/100;
        return "Flower{" +
                "flowerType=" + flowerType +
                ", color=" + color +
                ", length=" + length +
                ", price=" + price +
                ", isFlowerpot=" + isFlowerpot +
                '}';
    }
}
