package com.kutylo.model;

import com.kutylo.Utils.FlowerIO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Store {
    private List<Flower> inventory= new ArrayList<Flower>();
    private List<Bouqut> bouquets =new ArrayList<Bouqut>();

    Pattern addFlowerPattern = Pattern.compile("(?i)\\s*add\\s+(?<flowerType>\\w+?)\\s+(?<color>\\w+?)\\s+?(?<price>\\d+)");

    public Store(){
        FlowerIO flowerIO= new FlowerIO("inventory.json");
        try {
            inventory=flowerIO.readFromFile();
        }catch (IOException e){
            System.err.print(e);
        }
    }

    public List<Flower> getInventory() {
        return inventory;
    }

    public List<Flower> getSortedInventory() {
        sort(inventory);
        return inventory;
    }

    public void setInventory(List<Flower> inventory) {
        this.inventory = inventory;
    }

    public List<Bouqut> getBouquets() {
        return bouquets;
    }

    public void setBouquets(List<Bouqut> bouquets) {
        this.bouquets = bouquets;
    }

    public Bouqut createBougut() {
        Matcher matcher;
        String command;
        Scanner scanner = new Scanner(System.in);
        Bouqut bouqut=new Bouqut();

        System.out.println("Please enter flowers:");
        System.out.println("Format: add flowerType color price");
        System.out.println("Enter q for end");

        while (true) {
            command = scanner.nextLine();
            matcher = addFlowerPattern.matcher(command);

            if (command.equals("q")) {
                bouquets.add(bouqut);
                break;
            }

            if (matcher.find()) {
                //(flower.getLength().equals(Double.parseDouble(matcher.group("length")))) &&
                boolean isfound = false;

                for (Flower flower : inventory) {

                    if (flower.getFlowerType().matches("(?i)" + matcher.group("flowerType")) &&
                            (flower.getColor().matches("(?i)" + matcher.group("color")) &&
                                    (flower.getPrice().equals(Integer.parseInt(matcher.group("price")))))) {
                        if (flower.getFlowerpot()) {
                            isfound = true;
                            System.out.println("Its flowerpot. Please enter the flower for create bouqut!");
                            break;
                        }else {
                            bouqut.addFlower(flower);
                            isfound = true;
                            break;
                        }
                    }
                }
                if (!isfound) {
                    System.err.println("In inventory there's no such flower");
                }
            }
        }

        return bouqut;
    }

    private void sort(List<Flower> inventory){
        inventory.sort(new Comparator<Flower>() {
            public int compare(Flower a, Flower b) {
                int compare = a.getPrice().compareTo(b.getPrice());
                if (compare == 0) {
                    return 0;
                }
                if (compare > 0) {
                    return 1;
                }
                return -1;
            }
        });
    }


}
