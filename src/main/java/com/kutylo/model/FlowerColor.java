package com.kutylo.model;

public enum FlowerColor {
    GREEN, BLUE, RED, YELLOW
}
