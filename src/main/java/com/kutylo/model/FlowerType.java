package com.kutylo.model;

public enum FlowerType {
    ROSE, TULIP, ANEMORE, CHAMOMILE, NARCISSUS, MAGNOLIA
}
